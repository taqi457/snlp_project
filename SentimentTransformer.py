from sklearn.base import TransformerMixin, BaseEstimator
from pandas import DataFrame


class SentimentTransformer(BaseEstimator, TransformerMixin):

	def __init__(self, model):
		self.model = model

	def fit(self, *args, **kwargs):
		return self #We already have inbuilt model from Vader.

	def transform(self, X, **transform_params):
		result = []
		for tweet in X:
			# result.append(self.model.polarity_scores(tweet)['compound'])
			result.append(self.model.polarity_scores(tweet))
			# result.append(1) if self.model.polarity_scores(tweet)['compound']>-0.2 else result.append(0) #0 for offensive
		return DataFrame(result)