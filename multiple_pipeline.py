import os
import re
import sys

import numpy as np
import pandas as pd
from imblearn.metrics import classification_report_imbalanced
# from imblearn.pipeline import make_pipeline as make_pipeline_imb
from imblearn.pipeline import Pipeline
from imblearn.under_sampling import RandomUnderSampler
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.stem.porter import PorterStemmer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from sklearn import metrics
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import FeatureUnion

from SentimentTransformer import SentimentTransformer
from sklearn.model_selection import GridSearchCV

from ProfanityTransformer import ProfanityTransformer

tokenizer = RegexpTokenizer(r'\w+')
lemmatizer = WordNetLemmatizer()
stemmer = PorterStemmer()


def clean_text(tweet):
	text = tweet
	text = re.sub("@USER", "", text)
	text = re.sub(r'\bURL\b', "", text)

	return " ".join(tokenizer.tokenize(text))


def lemmatize_text(tweet):
	return " ".join([lemmatizer.lemmatize(w, 'v') for w in tweet.split(' ')])


def stem_text(tweet):
	return " ".join([stemmer.stem(w) for w in tweet.split(' ')])


class ItemSelector(BaseEstimator, TransformerMixin):
	"""For data grouped by feature, select subset of data at a provided key.

	The data is expected to be stored in a 2D data structure, where the first
	index is over features and the second is over samples.  i.e.

	>> len(data[key]) == n_samples

	Please note that this is the opposite convention to scikit-learn feature
	matrixes (where the first index corresponds to sample).

	ItemSelector only requires that the collection implement getitem
	(data[key]).  Examples include: a dict of lists, 2D numpy array, Pandas
	DataFrame, numpy record array, etc.

	>> data = {'a': [1, 5, 2, 5, 2, 8],
			   'b': [9, 4, 1, 4, 1, 3]}
	>> ds = ItemSelector(key='a')
	>> data['a'] == ds.transform(data)

	ItemSelector is not designed to handle data grouped by sample.  (e.g. a
	list of dicts).  If your data is structured this way, consider a
	transformer along the lines of `sklearn.feature_extraction.DictVectorizer`.

	Parameters
	----------
	key : hashable, required
		The key corresponding to the desired value in a mappable.
	"""

	def __init__(self, key):
		self.key = key

	def fit(self, x, y=None):
		return self

	def transform(self, data_dict):
		return data_dict[self.key]


class DataExtractor(BaseEstimator, TransformerMixin):
	"""Extract the subject & body from a usenet post in a single pass.

	Takes a sequence of strings and produces a dict of sequences.  Keys are
	`subject` and `body`.
	"""

	def fit(self, x, y=None):
		return self

	def transform(self, tweets):
		features = np.recarray(shape=(len(tweets),),
							   dtype=[('text', object), ('cleaned_text', object), ('lemmatized_text', object), ('stemmed_text', object)])
		for i, text in enumerate(tweets):
			features['text'][i] = text
			cleaned_text = clean_text(text)
			features['cleaned_text'][i] = cleaned_text
			features['lemmatized_text'][i] = lemmatize_text(cleaned_text)
			features['stemmed_text'][i] = stem_text(cleaned_text)
		return features


pipeline = Pipeline([
	('extracted_data', DataExtractor()),
	('union', FeatureUnion(transformer_list=[
		('simple_tfidf', Pipeline([
			('selector', ItemSelector(key='cleaned_text')),
			('tfidf',
			 TfidfVectorizer(analyzer='char_wb', ngram_range=(3, 6), use_idf=True, smooth_idf=True, sublinear_tf=True))
		])),
		('stemmed_tfidf', Pipeline([
			('selector', ItemSelector(key='stemmed_text')),
			('tfidf',
			 TfidfVectorizer(use_idf=True, smooth_idf=True, sublinear_tf=True))
		])),
		('lemmatized_tfidf', Pipeline([
			('selector', ItemSelector(key='lemmatized_text')),
			('tfidf',
			 TfidfVectorizer( use_idf=True, smooth_idf=True, sublinear_tf=True))
		])),
		('sentiment_analysis', Pipeline([
			('selector', ItemSelector(key='text')),
			('sentiment_analytics', SentimentTransformer(SentimentIntensityAnalyzer()))
		])),

		('profanity_analysis', Pipeline([
			('selector', ItemSelector(key='text')),
			('profanity_analysis', ProfanityTransformer())
		]))
	],
		transformer_weights=dict(simple_tfid=1, sentiment_analysis=0, profanity_analysis=0) #stemmed_tfid=0, lemmatized_tfid=0,
	)),
	('sampler', RandomUnderSampler()),
	('classifier', SGDClassifier(
		loss='squared_loss', penalty='l2', alpha=1e-3, random_state=69, max_iter=10, tol=None)
	 ),
])

folder_path = sys.argv[1]
test_dataset_path = os.path.join(folder_path, 'test.tsv')
train_path = os.path.join(folder_path, 'train.tsv')
dev_path = os.path.join(folder_path, 'dev.tsv')
train_data = pd.read_csv(train_path, sep='\t', usecols=[0, 1], names=['class_label', 'text'])
dev_data = pd.read_csv(dev_path, sep='\t', usecols=[0, 1], names=['class_label', 'text'])

# import ipdb; ipdb.set_trace()
pipeline.fit(train_data.text, train_data.class_label)
predicted_labels = pipeline.predict(dev_data.text)

# print(classification_report_imbalanced(dev_data.class_label, predicted_labels))
print(metrics.classification_report(dev_data.class_label, predicted_labels, target_names=['NOT', 'OFF']))

# possible_ngram = []
# for i in range(3, 8):
# 	for j in range(i + 2, i + 6):
# 		possible_ngram.append((i, j))
#
# possible_weights = []
# for weight1 in range(0, 20):
# 	for weight2 in range(0, 20):
# 		for weight4 in range(0, 20):
# 			possible_weights.append(dict(simple_tfidf=weight1 / 10,#  stemmed_tfid=weight2/10, lemmatized_tfid=weight3/10,
# 									 sentiment_analysis = weight2 / 10, profanity_analysis=weight4 / 10))

# possible_feature_counts = []
# for feature_count in range(100000, 1000000, 1000):
# 	possible_feature_counts.append(feature_count)
#
# param_grid = dict(
# 				  # union__simple_tfidf__tfidf__ngram_range=possible_ngram,
# 				  # union__simple_tfidf__tfidf__analyzer=['char', 'char_wb'],
# 				  union__transformer_weights=possible_weights
# 				  # union__simple_tfidf__tfidf__max_features = possible_feature_counts
# 				  )
# grid_search = GridSearchCV(pipeline, param_grid=param_grid, verbose=10, n_jobs=-1)
# grid_search.fit(dev_data.iloc[:, 1].tolist(), dev_data.iloc[:, 0].tolist())
# print(grid_search.best_estimator_)
# print(grid_search.best_score_)
# print(grid_search.best_params_)