from sklearn.base import TransformerMixin, BaseEstimator
from pandas import DataFrame
from profanity_check import predict_prob

class ProfanityTransformer(BaseEstimator, TransformerMixin):

	def fit(self, *args, **kwargs):
		return self

	def transform(self, X, **transform_params):
		return DataFrame(predict_prob(X))