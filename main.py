import os
import sys
import math
import operator
import re
from collections import defaultdict, Counter
import pandas as pd
import matplotlib.pyplot as plt
from nltk.tokenize import RegexpTokenizer
from imblearn.under_sampling import RandomUnderSampler
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
import numpy as np
from sklearn.feature_extraction.text import TfidfTransformer, TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import naive_bayes
from sklearn.linear_model import SGDClassifier
from sklearn import metrics
from sklearn.pipeline import Pipeline

def plot_pie(y):
    target_stats = Counter(y)
    labels = list(target_stats.keys())
    sizes = list(target_stats.values())
    explode = tuple([0.1] * len(target_stats))

    def make_autopct(values):
        def my_autopct(pct):
            total = sum(values)
            val = int(round(pct * total / 100.0))
            return '{p:.2f}%  ({v:d})'.format(p=pct, v=val)
        return my_autopct

    fig, ax = plt.subplots()
    ax.pie(sizes, explode=explode, labels=labels, shadow=True,
           autopct=make_autopct(sizes))
    ax.axis('equal')
    plt.show()


class DataPreparator:

    def __init__(self):
        self.count_vectorizer = CountVectorizer(analyzer='char_wb', ngram_range=(3, 6))
        self.tfidf_transformer = TfidfTransformer(use_idf=True, smooth_idf=True, sublinear_tf=True)
        self.text_classifier = SGDClassifier(
            loss='squared_loss', penalty='l2', alpha=1e-3, random_state=69, max_iter=10, tol=None
        )
        # self.text_classifier = naive_bayes.BernoulliNB()

    @staticmethod
    def undersample_dataset(feature_set, dependent_variable):
        rus = RandomUnderSampler(return_indices=True)
        sampled_feature_set, sampled_variable, id_rus = rus.fit_sample(feature_set, dependent_variable)
        return sampled_feature_set, sampled_variable

    def prepare_dataset(self, file_path):
        # dataset = pd.read_csv(file_path, sep='TfidfTransformer\t', usecols=[0, 1], nrows=10, names=['class_label', 'text'])
        dataset = pd.read_csv(file_path, sep='\t', usecols=[0, 1], names=['class_label', 'text'])
        dataset['clean_text'] = dataset.text.apply(lambda text: self.clean_text(text))#Where do we use this?
        dataset['class_label'] = LabelEncoder().fit_transform(dataset.class_label)#Where do we use this?
        training_counts = self.count_vectorizer.fit_transform(dataset.clean_text)
        tfidf_feature_set = self.tfidf_transformer.fit_transform(training_counts)
        feature_set, class_label = self.undersample_dataset(tfidf_feature_set, dataset.class_label)
        return feature_set, class_label

    def split_dataset(self, feature_set, dependent_variable):
        sampled_feature_set, sampled_variable = self.undersample_dataset(feature_set, dependent_variable)
        k_folder = KFold(n_splits=10, random_state=42, shuffle=False)
        return k_folder.split(sampled_feature_set)

    def train(self, file_path):
        test_feature_feature_set, test_dependent_variable = self.prepare_dataset(file_path)
        predictor = self.text_classifier.fit(test_feature_feature_set, test_dependent_variable)
        return predictor

    def test(self, file_path, predictor):
        # dataset = pd.read_csv(file_path, sep='\t', usecols=[0, 1], nrows=10, names=['class_label', 'text'])
        dataset = pd.read_csv(file_path, sep='\t', usecols=[0, 1], names=['class_label', 'text'])
        dataset['clean_text'] = dataset.text.apply(lambda text: self.clean_text(text))
        dataset['class_label'] = LabelEncoder().fit_transform(dataset.class_label)
        testing_counts = self.count_vectorizer.transform(dataset.clean_text)
        test_feature_set = self.tfidf_transformer.transform(testing_counts)
        predicted = predictor.predict(test_feature_set)
        print(metrics.classification_report(dataset.class_label, predicted, target_names=['NOT', 'OFF']))
        print("The accuracy is: %s" % metrics.accuracy_score(dataset.class_label, predicted))

    def run_kfold(self, split_indices, feature_set, dependent_variable):
        result = []
        for train_index, test_index in split_indices:
            train_feature_set, train_dependent_variable, \
            test_feature_set, test_dependent_variable = feature_set[train_index], dependent_variable[train_index], \
                feature_set[test_index], dependent_variable[test_index],
            predictor = self.text_classifier.fit(train_feature_set, train_dependent_variable)
            predicted = predictor.predict(test_feature_set)
            result.append(np.mean(predicted == test_dependent_variable))
            print(metrics.classification_report(test_dependent_variable, predicted, target_names=['OFF', 'NON']))

        print(result)
        print(np.mean(result))

    @staticmethod
    def clean_text(tweet):
        text = tweet
        text = re.sub("@USER", "", tweet)
        text = re.sub(r'\bURL\b', "", text)
        # text = text.lower()
        # return text.split(' ')     # text = text.lower() #' We also observed that the performance
        #     # of the classifier has degraded when the tokens were lower-cased, which suggests that word shapes
        #     # are informative for this task.'
        tokenizer = RegexpTokenizer(r'\w+')
        return " ".join(tokenizer.tokenize(text))
        # return text


def main(folder_path):
    test_path = os.path.join(folder_path, 'test.tsv')
    train_path = os.path.join(folder_path, 'train.tsv')
    dev_path = os.path.join(folder_path, 'dev.tsv')
    dp = DataPreparator()
    # feature_set, dependent_variable = dp.prepare_dataset(train_path)
    # indices_generator = dp.split_dataset(feature_set, dependent_variable)
    # dp.run_kfold(indices_generator, feature_set, dependent_variable)
    predictor = dp.train(train_path)
    dp.test(dev_path, predictor)



main(sys.argv[1])